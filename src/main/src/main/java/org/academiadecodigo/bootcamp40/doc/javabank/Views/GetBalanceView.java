package org.academiadecodigo.bootcamp40.doc.javabank.Views;

import org.academiadecodigo.bootcamp40.doc.javabank.models.account.Account;

public class GetBalanceView extends AbstractView {


    public void showAccountsBalance(Account account, double accountBalance) {

        System.out.println("Account ID: " + account + "\n" + "Current balance: " + df.format(accountBalance) + "\n\n");

    }

}
