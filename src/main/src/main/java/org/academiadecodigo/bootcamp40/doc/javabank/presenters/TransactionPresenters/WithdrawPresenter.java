package org.academiadecodigo.bootcamp40.doc.javabank.presenters.TransactionPresenters;

public class WithdrawPresenter extends AbstractTransactionPresenter{


    @Override
    public void submitTransaction(int accountID, double amount) {

       try {
           accountService.withdraw(accountID, amount);
       } catch (Exception e){
           accountTransactionView.printNoFundsError();
       }

       accountTransactionView.printSuccessMessage(accountID);
    }
}
