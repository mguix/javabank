package org.academiadecodigo.bootcamp40.doc.javabank.presenters;

import org.academiadecodigo.bootcamp40.doc.javabank.services.AuthenticationService;
import org.academiadecodigo.bootcamp40.doc.javabank.Views.LoginView;

public class LoginPresenter extends AbstractPresenter {

    private AuthenticationService authenticationService;
    private LoginView loginView;
    private SignUpPresenter signUpPresenter;
    private MainMenuPresenter mainMenuPresenter;
    private int loginAttempts;

    public LoginPresenter() {
        loginAttempts = 0;
    }

    public void start() {

        int indexFromInput = loginView.start();

        switch (indexFromInput) {
            case 1:

                try {
                    login();
                } catch (IllegalAccessException e){
                    loginView.printTooManyAttempts();
                }

            case 2:
                signUpPresenter.start();
        }
    }

    private void login() throws IllegalAccessException {

        String customerID = loginView.getCustomerID();
        String customerPassword = loginView.getCustomerPassword();
        boolean successfulLogin = authenticationService.authenticateCustomer(customerID, customerPassword);

        if (loginAttempts >= 3) {
            loginAttempts = 0;
            throw new IllegalAccessException();
        }

        if (successfulLogin) {
            loginView.printUnsuccessfulLogin();
            loginAttempts++;
            login();

        }

        loginView.printSuccessfulLogin(authenticationService.getCustomerName());
        mainMenuPresenter.start();

    }

    public void setAuthenticationService(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    public void setLoginView(LoginView loginView) {
        this.loginView = loginView;
    }

    public void setSignUpPresenter(SignUpPresenter signUpPresenter) {
        this.signUpPresenter = signUpPresenter;
    }

    public void setMainMenuPresenter(MainMenuPresenter mainMenuPresenter) {
        this.mainMenuPresenter = mainMenuPresenter;
    }

}
