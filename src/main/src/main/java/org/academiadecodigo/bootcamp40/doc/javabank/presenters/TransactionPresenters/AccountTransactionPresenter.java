package org.academiadecodigo.bootcamp40.doc.javabank.presenters.TransactionPresenters;

import org.academiadecodigo.bootcamp40.doc.javabank.presenters.Presenter;

public interface AccountTransactionPresenter extends Presenter {

    void submitTransaction(int accountID, double amount);

}
