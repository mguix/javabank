package org.academiadecodigo.bootcamp40.doc.javabank.factory;

import org.academiadecodigo.bootcamp40.doc.javabank.models.account.Account;
import org.academiadecodigo.bootcamp40.doc.javabank.models.account.AccountType;
import org.academiadecodigo.bootcamp40.doc.javabank.models.account.CheckingAccount;
import org.academiadecodigo.bootcamp40.doc.javabank.models.account.SavingsAccount;

public class AccountFactory {

     /**
     * Creates a new {@link Account}
     *
     * @param accountType the account type
     * @return the new account
     */
    public Account createAccount(AccountType accountType) {

        Account newAccount;
        switch (accountType) {
            case CHECKING:
                newAccount = new CheckingAccount();
                break;
            case SAVINGS:
                newAccount = new SavingsAccount();
                break;
            default:
                newAccount = null;

        }

        return newAccount;
    }

}
