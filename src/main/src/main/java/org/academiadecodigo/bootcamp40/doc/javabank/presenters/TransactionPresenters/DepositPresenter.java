package org.academiadecodigo.bootcamp40.doc.javabank.presenters.TransactionPresenters;

public class DepositPresenter extends AbstractTransactionPresenter {


    @Override
    public void submitTransaction(int accountID, double amount) {
        accountService.deposit(accountID, amount);
    }
}
