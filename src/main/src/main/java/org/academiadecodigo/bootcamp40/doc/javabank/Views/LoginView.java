package org.academiadecodigo.bootcamp40.doc.javabank.Views;

import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

public class LoginView extends AbstractView {

    public int start() {

        String[] options = {"Login", "Sign in"};
        MenuInputScanner loginOrSignIn = new MenuInputScanner(options);
        loginOrSignIn.setMessage(Messages.LOGIN_OR_SIGN_UP);
        return prompt.getUserInput(loginOrSignIn);
    }

    public String getCustomerID() {

        StringInputScanner loginCustomerID = new StringInputScanner();
        loginCustomerID.setMessage(Messages.CUSTOMER_ID);
        return prompt.getUserInput(loginCustomerID);
    }

    public String getCustomerPassword() {

        PasswordInputScanner loginCustomerPassword = new PasswordInputScanner();
        loginCustomerPassword.setMessage(Messages.CUSTOMER_PASSWORD);
        return prompt.getUserInput(loginCustomerPassword);
    }

    public void printUnsuccessfulLogin() {
        System.out.println(Messages.LOGIN_UNSUCCESSFUL);
    }

    public void printSuccessfulLogin(String customerName) {
        System.out.println(Messages.LOGIN_SUCCESSFUL + customerName);
    }

    public void printTooManyAttempts() {
        System.out.println(Messages.TOO_MANY_ATTEMPTS);
    }

}
