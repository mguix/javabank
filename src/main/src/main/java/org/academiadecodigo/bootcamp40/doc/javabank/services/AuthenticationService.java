package org.academiadecodigo.bootcamp40.doc.javabank.services;

public class AuthenticationService {

    private CustomerService customerService;

    public void setCustomerService(CustomerService customerService){
        this.customerService = customerService;
    }

    public boolean authenticateCustomer(String customerID, String customerPassword){

        return customerService.getCustomer(customerID, customerPassword) != null;
    }

    public boolean authenticateCustomerID(String customerID){

        return customerService.checkCustomerID(customerID);
    }

    public String getCustomerName(){
        return customerService.getCurrentCustomer().getCustomerName();
    }

}
