package org.academiadecodigo.bootcamp40.doc.javabank.presenters;

import org.academiadecodigo.bootcamp40.doc.javabank.factory.AccountFactory;
import org.academiadecodigo.bootcamp40.doc.javabank.Views.OpenAccountView;
import org.academiadecodigo.bootcamp40.doc.javabank.models.account.Account;
import org.academiadecodigo.bootcamp40.doc.javabank.models.account.AccountType;
import org.academiadecodigo.bootcamp40.doc.javabank.services.AccountService;
import org.academiadecodigo.bootcamp40.doc.javabank.services.CustomerService;

import java.util.Set;

public class OpenAccountPresenter extends AbstractPresenter {

    private CustomerService customerService;
    private AccountService accountService;
    private OpenAccountView openAccountView;
    private AccountFactory accountFactory;

    @Override
    public void start() {

        int userInput = openAccountView.chooseAccountType();
        int randomNum = getRandomNumber();

        switch (userInput){

            case 1:
                Account newCheckingAccount = accountFactory.createAccount(AccountType.CHECKING);
                customerService.getCurrentCustomer().addAccount(newCheckingAccount, randomNum);
                accountService.addAccount(randomNum, newCheckingAccount);
                openAccountView.printNewAccount(randomNum);
                break;
            case 2:
                Account newSavingsAccount = accountFactory.createAccount(AccountType.SAVINGS);
                customerService.getCurrentCustomer().addAccount(newSavingsAccount, randomNum);
                accountService.addAccount(randomNum, newSavingsAccount);
                openAccountView.printNewAccount(randomNum);
                break;
        }
    }

    private int getRandomNumber() {
        //this will also have to check that there's no other account with the same number

        int randomNum = (int) Math.random() * 9999;
        Set<Account> customerAccounts = customerService.getCustomerAccounts();

        for (Account a : customerAccounts){

            if (randomNum == a.getAccountID()){
                getRandomNumber();
            }

        }

        return randomNum;
    }

    public void setCustomerService(CustomerService customerService){
        this.customerService = customerService;
    }

    public void setAccountService(AccountService accountService){
        this.accountService = accountService;
    }

    public void setOpenAccountView(OpenAccountView openAccountView){
        this.openAccountView = openAccountView;
    }

    public void setAccountFactory(AccountFactory accountFactory){
        this.accountFactory = accountFactory;
    }

}
