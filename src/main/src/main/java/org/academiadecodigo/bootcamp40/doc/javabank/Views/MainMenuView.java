package org.academiadecodigo.bootcamp40.doc.javabank.Views;

import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

public class MainMenuView extends AbstractView {


    public int showOperations(){

        String[] options = {"View Balance", "Make Deposit", "Make Withdrawal", "Open Account", "Quit"};
        MenuInputScanner startMenu = new MenuInputScanner(options);

        startMenu.setMessage(Messages.CHOOSE_OPERATION);
        return prompt.getUserInput(startMenu);
    }

    public void printLogout(){
        System.out.println(Messages.LOGOUT_SUCCESSFUL);
    }

}
