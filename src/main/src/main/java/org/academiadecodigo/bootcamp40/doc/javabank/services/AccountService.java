package org.academiadecodigo.bootcamp40.doc.javabank.services;

import org.academiadecodigo.bootcamp40.doc.javabank.models.account.Account;

import java.util.HashMap;
import java.util.Map;


/**
 * Responsible for account management
 */
public class AccountService {

    private static int numberAccounts = 0;
    private Map<Integer, Account> accountMap;

    /**
     * Creates a new {@code AccountService}
     */
    public AccountService() {
        this.accountMap = new HashMap<>();
    }

    public void addAccount(int accountID, Account account){
        accountMap.put(accountID, account);
    }

    public Map<Integer, Account> getAccounts(){
        return accountMap;
    }

    /**
     * Perform an {@link Account} deposit if possible
     *
     * @param id     the id of the account
     * @param amount the amount to deposit
     */
    public void deposit(int id, double amount) {
        accountMap.get(id).credit(amount);
    }

    /**
     * Perform an {@link Account} withdrawal if possible
     *
     * @param id     the id of the account
     * @param amount the amount to withdraw
     */
    public void withdraw(int id, double amount) throws Exception {

        Account account = accountMap.get(id);

        if (!account.canWithdraw()) {
            throw new Exception();
        }

        accountMap.get(id).debit(amount);
    }

    /**
     * Performs a transfer between two {@link Account} if possible
     *
     * @param srcId  the source account id
     * @param dstId  the destination account id
     * @param amount the amount to transfer
     */
    public void transfer(int srcId, int dstId, double amount) {

        Account srcAccount = accountMap.get(srcId);
        Account dstAccount = accountMap.get(dstId);

        // make sure transaction can be performed
        if (srcAccount.canDebit(amount) && dstAccount.canCredit(amount)) {
            srcAccount.debit(amount);
            dstAccount.credit(amount);
        }
    }
}
