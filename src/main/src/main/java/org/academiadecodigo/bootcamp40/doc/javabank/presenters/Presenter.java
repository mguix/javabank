package org.academiadecodigo.bootcamp40.doc.javabank.presenters;

public interface Presenter {

    void start();

}
