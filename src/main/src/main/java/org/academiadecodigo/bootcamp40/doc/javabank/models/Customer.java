package org.academiadecodigo.bootcamp40.doc.javabank.models;

import org.academiadecodigo.bootcamp40.doc.javabank.models.account.Account;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * The customer models entity
 */

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Integer id;

    @Version
    private Integer version;

    @CreationTimestamp
    private Date creationTime;

    @UpdateTimestamp
    private Date updateTime;

    @OneToMany(
            cascade = {CascadeType.ALL},
            mappedBy = "customer"
    )

    private Set<Account> accounts = new HashSet<>();
    private String customerName;
    private String customerID;
    private String password;

    public Customer(String customerName, String customerID, String password){
        this.customerName = customerName;
        this.customerID = customerID;
        this.password = password;
    }

    /**
     * Adds a new account
     *
     * @param account the account to be opened
     */
    public void addAccount(Account account, int accountID) {
        accounts.add(account);
        account.setAccountID(accountID);
    }

    public Set<Account> getCustomerAccounts(){
        return accounts;
    }

    public String getCustomerID(){
        return customerID;
    }

    public String getPassword(){
        return password;
    }

    public String getCustomerName(){
        return customerName;
    }



}
