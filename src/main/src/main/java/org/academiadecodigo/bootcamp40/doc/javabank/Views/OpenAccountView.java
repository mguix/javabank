package org.academiadecodigo.bootcamp40.doc.javabank.Views;

import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

public class OpenAccountView extends AbstractView {

    public int chooseAccountType(){

        String[] options = {"Checking Account", "Savings Account"};
        MenuInputScanner chooseAccountType = new MenuInputScanner(options);
        chooseAccountType.setMessage(Messages.CHOOSE_ACCOUNT_TYPE);

        return prompt.getUserInput(chooseAccountType);
    }

    public void printNewAccount(int accountID){
        System.out.println(Messages.NEW_OPENED_ACCOUNT + accountID);
    }


}
