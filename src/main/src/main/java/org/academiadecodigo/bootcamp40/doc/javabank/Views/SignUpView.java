package org.academiadecodigo.bootcamp40.doc.javabank.Views;

import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

public class SignUpView extends AbstractView {

    public String getNewCustomerName() {

        StringInputScanner newCustomerName = new StringInputScanner();
        newCustomerName.setMessage(Messages.ENTER_NAME);
        return prompt.getUserInput(newCustomerName);
    }

    public String getNewCustomerID() {

        StringInputScanner newCustomerID = new StringInputScanner();
        newCustomerID.setMessage(Messages.ENTER_ID);
        return prompt.getUserInput(newCustomerID);
    }

    public String getNewCustomerPassword() {

        PasswordInputScanner newCustomerPassword = new PasswordInputScanner();
        newCustomerPassword.setMessage(Messages.ENTER_PASSWORD);
        return prompt.getUserInput(newCustomerPassword);
    }

    public String confirmNewCustomerPassword() {

        PasswordInputScanner confirmNewCustomerPassword = new PasswordInputScanner();
        confirmNewCustomerPassword.setMessage(Messages.REPEAT_PASSWORD);
        return prompt.getUserInput(confirmNewCustomerPassword);
    }

    public void printUserAlreadyExists() {
        System.out.println(Messages.USER_ALREADY_EXISTS);
    }

    public void printUnmatchingPassword() {
        System.out.println(Messages.UNMATCHING_PASSWORD);
    }

    public void printSignUpSuccessful(String customerName) {
        System.out.println(Messages.SIGN_UP_SUCCESSFUL + customerName);
    }
}
