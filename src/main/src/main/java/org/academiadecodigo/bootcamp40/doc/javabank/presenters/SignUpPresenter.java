package org.academiadecodigo.bootcamp40.doc.javabank.presenters;

import org.academiadecodigo.bootcamp40.doc.javabank.Views.SignUpView;
import org.academiadecodigo.bootcamp40.doc.javabank.models.Customer;
import org.academiadecodigo.bootcamp40.doc.javabank.services.AuthenticationService;
import org.academiadecodigo.bootcamp40.doc.javabank.services.CustomerService;

public class SignUpPresenter extends AbstractPresenter{

    private CustomerService customerService;
    private AuthenticationService authenticationService;
    private SignUpView signUpView;
    private LoginPresenter loginPresenter;
    private MainMenuPresenter mainMenuPresenter;

    public void start() {

        String customerID = signUpView.getNewCustomerID();
        boolean customerAlreadyExists = authenticationService.authenticateCustomerID(customerID);

        if (customerAlreadyExists) {
            signUpView.printUserAlreadyExists();
            loginPresenter.start();
            return;
        }

        String customerName = signUpView.getNewCustomerName();
        String customerPassword = setPassword();

        Customer customer = new Customer(customerName, customerID, customerPassword);
        customerService.setCurrentCustomer(customer);
        customerService.addCustomer(customer);
        signUpView.printSignUpSuccessful(authenticationService.getCustomerName());

        mainMenuPresenter.start();
    }

    private String setPassword() {

        String customerPassword = signUpView.getNewCustomerPassword();

        String confirmCustomerPassword = signUpView.confirmNewCustomerPassword();

        if (!(customerPassword.equals(confirmCustomerPassword))) {

            signUpView.printUnmatchingPassword();
            setPassword();
        }

        return customerPassword;
    }

    public void setCustomerService(CustomerService customerService){
        this.customerService = customerService;
    }

    public void setAuthenticationService(AuthenticationService authenticationService){
        this.authenticationService = authenticationService;
    }

    public void setSignUpView(SignUpView signUpView){
        this.signUpView = signUpView;
    }

    public void setLoginPresenter(LoginPresenter loginPresenter){
        this.loginPresenter = loginPresenter;
    }

    public void setMainMenuPresenter(MainMenuPresenter mainMenuPresenter){
        this.mainMenuPresenter = mainMenuPresenter;
    }

}
