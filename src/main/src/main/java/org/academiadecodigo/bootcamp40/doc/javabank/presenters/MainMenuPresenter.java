package org.academiadecodigo.bootcamp40.doc.javabank.presenters;

import org.academiadecodigo.bootcamp40.doc.javabank.presenters.TransactionPresenters.DepositPresenter;
import org.academiadecodigo.bootcamp40.doc.javabank.presenters.TransactionPresenters.TransferencePresenter;
import org.academiadecodigo.bootcamp40.doc.javabank.presenters.TransactionPresenters.WithdrawPresenter;
import org.academiadecodigo.bootcamp40.doc.javabank.Views.MainMenuView;

public class MainMenuPresenter extends AbstractPresenter {

    private MainMenuView mainMenuView;
    private GetBalancePresenter getBalancePresenter;
    private DepositPresenter depositPresenter;
    private WithdrawPresenter withdrawPresenter;
    private TransferencePresenter transferencePresenter;
    private OpenAccountPresenter openAccountPresenter;


    public void start() {

        int chosenOperation = mainMenuView.showOperations();

        switch (chosenOperation) {

            case 1:
                getBalancePresenter.start();
                start();
                break;
            case 2:
                depositPresenter.start();
                start();
                break;
            case 3:
                withdrawPresenter.start();
                start();
                break;
            case 4:
                transferencePresenter.start();
                start();
                break;
            case 5:
                openAccountPresenter.start();
                start();
                break;
            case 6:
               // deleteAccount();
                start();
                break;
            case 7:
                // customer == null?
                mainMenuView.printLogout();
                break;
        }

    }

    public void setMainMenuView(MainMenuView mainMenuView){
        this.mainMenuView = mainMenuView;
    }

    public void setGetBalancePresenter(GetBalancePresenter getBalancePresenter){
        this.getBalancePresenter = getBalancePresenter;
    }

    public void setDepositPresenter(DepositPresenter depositPresenter){
        this.depositPresenter = depositPresenter;
    }

    public void setWithdrawPresenter(WithdrawPresenter withdrawPresenter){
        this.withdrawPresenter = withdrawPresenter;
    }

    public void setTransferencePresenter(TransferencePresenter transferencePresenter){
        this.transferencePresenter = transferencePresenter;
    }

    public void setOpenAccountPresenter(OpenAccountPresenter openAccountPresenter){
        this.openAccountPresenter = openAccountPresenter;
    }

}
