package org.academiadecodigo.bootcamp40.doc.javabank;

import org.academiadecodigo.bootcamp40.doc.javabank.presenters.LoginPresenter;
import org.academiadecodigo.bootcamp40.doc.javabank.services.AccountService;
import org.academiadecodigo.bootcamp40.doc.javabank.services.AuthenticationService;
import org.academiadecodigo.bootcamp40.doc.javabank.services.CustomerService;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class BankApp {

    public static void main(String[] args) {

        BankApp bankApp = new BankApp();
        bankApp.bootstrap();

    }

    private void bootstrap(){

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("javabank");
        DbManager dbManager = new DbManager(emf);

        Bootstrap bootstrap = new Bootstrap();
        bootstrap.setAuthenticationService(new AuthenticationService());
        bootstrap.setCustomerService(new CustomerService());
        bootstrap.setAccountService(new AccountService());

        LoginPresenter loginPresenter = bootstrap.wireObjects();
        loginPresenter.start();

        //Always remember to close the Entity Manager Factory when all the rest of the tasks are done
        emf.close();

    }

}
