package org.academiadecodigo.bootcamp40.doc.javabank.presenters;

import org.academiadecodigo.bootcamp40.doc.javabank.Views.GetBalanceView;
import org.academiadecodigo.bootcamp40.doc.javabank.models.account.Account;
import org.academiadecodigo.bootcamp40.doc.javabank.services.CustomerService;

import java.util.Map;
import java.util.Set;

public class GetBalancePresenter extends AbstractPresenter {

    private CustomerService customerService;
    private GetBalanceView getBalanceView;

    public void start() {

        Set<Account> customerAccounts = customerService.getCustomerAccounts();

        for (Account a : customerAccounts) {

            getBalanceView.showAccountsBalance(a, a.getBalance());
        }
    }

    public void setCustomerService(CustomerService customerService){
        this.customerService = customerService;
    }

    public void setGetBalanceView(GetBalanceView getBalanceView){
        this.getBalanceView = getBalanceView;
    }

}
