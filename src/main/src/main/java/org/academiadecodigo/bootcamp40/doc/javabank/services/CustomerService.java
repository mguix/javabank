package org.academiadecodigo.bootcamp40.doc.javabank.services;

import org.academiadecodigo.bootcamp40.doc.javabank.models.Customer;
import org.academiadecodigo.bootcamp40.doc.javabank.models.account.Account;

import java.util.HashSet;
import java.util.Set;

/**
 * The CustomerService entity
 */
public class CustomerService {

    private AccountService accountService;
    private Set<Customer> customers = new HashSet<>();
    private Customer currentCustomer;

    /**
     * Adds a new customer to the bank
     *
     * @param customer the new bank customer
     */
    public void addCustomer(Customer customer) {
        customers.add(customer);
    }

    public Customer getCustomer(String customerID, String customerPassword){

        for (Customer c : customers){

            if (c.getCustomerID().equals(customerID) && c.getPassword().equals(customerPassword)){
                currentCustomer = c;
                return c;
            }
        }

        return null;
    }

    public boolean checkCustomerID(String customerID){

        for (Customer c: customers){
            if(c.getCustomerID().equals(customerID)){
                return true;
            }
        }

        return false;
    }

    public Set<Account> getCustomerAccounts(){
        return currentCustomer.getCustomerAccounts();
    }

    /**
     * Gets the balance of an {@link Account}
     *
     * @param id the id of the account
     * @return the account balance
     */
    public double getAccountBalance(int id) {
        return accountService.getAccounts().get(id).getBalance();
    }

    /**
     * Gets the total customer balance
     *
     * @return the customer balance
     */
    public double getTotalCustomerBalance() {

        double balance = 0;

        for (Account account : currentCustomer.getCustomerAccounts()) {
            balance += account.getBalance();
        }

        return balance;
    }

    public Customer getCurrentCustomer(){
        return currentCustomer;
    }

    public void setCurrentCustomer(Customer currentCustomer){
        this.currentCustomer = currentCustomer;
    }

    public void setAccountService(AccountService accountService){
        this.accountService = accountService;
    }


}
