package org.academiadecodigo.bootcamp40.doc.javabank.Views;


import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.precisiondouble.DoubleInputScanner;
import org.academiadecodigo.bootcamp40.doc.javabank.models.account.Account;

public class AccountTransactionView extends AbstractView {

    public void showAccountsBalance(Account account, double accountBalance) {

        System.out.println("Account ID: " + account + "\n" + "Current balance: " + df.format(accountBalance) + "\n\n");
    }

    public int chooseAccount() {

        IntegerInputScanner chooseDepositAccount = new IntegerInputScanner();
        chooseDepositAccount.setMessage(Messages.ENTER_ACCOUNT_ID);

        return prompt.getUserInput(chooseDepositAccount);
    }

    public double chooseAmount() {

        DoubleInputScanner chooseAmountToDeposit = new DoubleInputScanner();
        chooseAmountToDeposit.setMessage(Messages.ENTER_AMOUNT);

        return prompt.getUserInput(chooseAmountToDeposit);
    }

    public void printSuccessMessage(int accountID) {
        System.out.println(Messages.TRANSACTION_SUCCESSFUL + accountID);
    }

    public void printInvalidID() {
        System.out.println(Messages.ACCOUNT_ID_NOT_VALID);
    }

    public void printNoFundsError() {
        System.out.println(Messages.NOT_ENOUGH_FUNDS);
    }

    public void printNewBalance(double newBalance) {
        System.out.println("New account balance: " + df.format(newBalance));
    }
}
