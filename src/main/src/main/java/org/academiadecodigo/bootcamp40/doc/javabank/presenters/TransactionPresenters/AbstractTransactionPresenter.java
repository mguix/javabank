package org.academiadecodigo.bootcamp40.doc.javabank.presenters.TransactionPresenters;

import org.academiadecodigo.bootcamp40.doc.javabank.presenters.AbstractPresenter;
import org.academiadecodigo.bootcamp40.doc.javabank.Views.AccountTransactionView;
import org.academiadecodigo.bootcamp40.doc.javabank.models.account.Account;
import org.academiadecodigo.bootcamp40.doc.javabank.services.AccountService;

import java.util.Map;

public abstract class AbstractTransactionPresenter extends AbstractPresenter implements AccountTransactionPresenter {

    protected AccountService accountService;
    protected AccountTransactionView accountTransactionView;
    private Map<Integer, Account> customerAccounts;



    public void start() {

        customerAccounts = accountService.getAccounts();

        for (Account a : customerAccounts.values()) {
            accountTransactionView.showAccountsBalance(a, a.getBalance());
        }

        chooseAccount();
    }

    private void chooseAccount() {

        int userInput = accountTransactionView.chooseAccount();

        for (Account a : customerAccounts.values()) {

            if (a.getAccountID() == userInput) {
                int accountID = accountTransactionView.chooseAccount();
                double amount = accountTransactionView.chooseAmount();
                submitTransaction(accountID, amount);
                accountTransactionView.printNewBalance(accountService.getAccounts().get(accountID).getBalance());
                return;
            }
        }

        accountTransactionView.printInvalidID();
    }

    public abstract void submitTransaction(int accountID, double amount);

    public void setAccountTransactionView(AccountTransactionView accountTransactionView) {
        this.accountTransactionView = accountTransactionView;
    }

    public void setAccountService(AccountService accountService){
        this.accountService = accountService;
    }

}
