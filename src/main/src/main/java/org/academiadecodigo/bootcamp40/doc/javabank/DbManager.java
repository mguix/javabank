package org.academiadecodigo.bootcamp40.doc.javabank;

import org.academiadecodigo.bootcamp40.doc.javabank.models.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.RollbackException;

public class DbManager {

    EntityManagerFactory emf;

    public DbManager(EntityManagerFactory emf){
        this.emf = emf;
    }

    public void mapping(Customer customer){

        EntityManager em = emf.createEntityManager();

        try {

            em.getTransaction().begin();
            em.persist(customer);
            em.getTransaction().commit();

        } catch (RollbackException e){
            em.getTransaction().rollback();

        } finally {
            em.close();
        }

    }

    public Customer findById(Integer id) {

        // open a new connection to the database
        EntityManager em = emf.createEntityManager();

        try {
            // fetch a new user using its id
            return em.find(Customer.class, id); // always the primary key

        } finally {
            // make sure we close the database connection
            if (em != null) {
                em.close();
            }
        }
    }
}
