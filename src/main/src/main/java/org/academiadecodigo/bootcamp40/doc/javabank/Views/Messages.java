package org.academiadecodigo.bootcamp40.doc.javabank.Views;

class Messages {

    //LOGIN VIEW MESSAGES
    static final String LOGIN_OR_SIGN_UP = "Sign in to create a new account or login if you already have one: ";
    static final String CUSTOMER_ID = "Please insert your costumer ID: ";
    static final String CUSTOMER_PASSWORD = "Please insert your password: ";
    static final String LOGIN_SUCCESSFUL = "You've logged in successfully, welcome ";
    static final String LOGIN_UNSUCCESSFUL = "Sorry, your ID and/or password are incorrect";
    static final String TOO_MANY_ATTEMPTS = "You have acceded the maximum number of attempts to login, please try again later";

    //SIGN UP VIEW MESSAGES
    static final String ENTER_NAME = "Enter your name: ";
    static final String ENTER_ID = "Please enter your ID: ";
    static final String ENTER_PASSWORD = "Please insert your new password: ";
    static final String REPEAT_PASSWORD = "Insert your new password again: ";
    static final String USER_ALREADY_EXISTS = "You already have an account with this ID, please login, or sign up with a new ID";
    static final String UNMATCHING_PASSWORD = "Your passwords don't match, please try again";
    static final String SIGN_UP_SUCCESSFUL = "You successfully signed up, welcome ";

    //MAIN MENU VIEW MESSAGE
    static final String CHOOSE_OPERATION = "What do you want to do next? ";

    //ACCOUNT TRANSACTION VIEW MESSAGES
    static final String ENTER_ACCOUNT_ID = "Please insert the ID of the account: ";
    static final String ENTER_AMOUNT = "Insert the amount: ";
    static final String TRANSACTION_SUCCESSFUL = "Your transaction has been made successfully with your account: ";
    static final String ACCOUNT_ID_NOT_VALID = "The ID you introduced is not a valid ID, please choose one of the accounts above";
    static final String NOT_ENOUGH_FUNDS = "You don't have enough funds to withdraw this amount";

    //OPEN ACCOUNT VIEW MESSAGES
    static final String CHOOSE_ACCOUNT_TYPE = "Please choose the type of account you wish to open: ";
    static final String NEW_OPENED_ACCOUNT = "Your new account has been opened with ID: ";

    //LOGOUT
    static final String LOGOUT_SUCCESSFUL = "You've logged out, see you soon";

}
