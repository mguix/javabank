package org.academiadecodigo.bootcamp40.doc.javabank;

import org.academiadecodigo.bootcamp40.doc.javabank.Views.*;
import org.academiadecodigo.bootcamp40.doc.javabank.presenters.*;
import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp40.doc.javabank.factory.AccountFactory;
import org.academiadecodigo.bootcamp40.doc.javabank.presenters.TransactionPresenters.DepositPresenter;
import org.academiadecodigo.bootcamp40.doc.javabank.presenters.TransactionPresenters.TransferencePresenter;
import org.academiadecodigo.bootcamp40.doc.javabank.presenters.TransactionPresenters.WithdrawPresenter;
import org.academiadecodigo.bootcamp40.doc.javabank.services.AccountService;
import org.academiadecodigo.bootcamp40.doc.javabank.services.AuthenticationService;
import org.academiadecodigo.bootcamp40.doc.javabank.services.CustomerService;

public class Bootstrap {

    private AuthenticationService authenticationService;
    private CustomerService customerService;
    private AccountService accountService;

    public void setAuthenticationService(AuthenticationService authenticationService){
        this.authenticationService = authenticationService;
    }

    public void setCustomerService(CustomerService customerService){
        this.customerService = customerService;
    }

    public void setAccountService(AccountService accountService){
        this.accountService = accountService;
    }

    public LoginPresenter wireObjects(){

        Prompt prompt = new Prompt(System.in, System.out);

        authenticationService.setCustomerService(customerService);
        customerService.setAccountService(accountService);

        LoginPresenter loginPresenter = new LoginPresenter();
        loginPresenter.setAuthenticationService(authenticationService);
        LoginView loginView = new LoginView();
        loginView.setPrompt(prompt);
        loginPresenter.setLoginView(loginView);

        SignUpPresenter signUpPresenter = new SignUpPresenter();
        signUpPresenter.setCustomerService(customerService);
        signUpPresenter.setAuthenticationService(authenticationService);
        SignUpView signUpView = new SignUpView();
        signUpView.setPrompt(prompt);
        signUpPresenter.setSignUpView(signUpView);
        signUpPresenter.setLoginPresenter(loginPresenter);
        loginPresenter.setSignUpPresenter(signUpPresenter);

        MainMenuPresenter mainMenuPresenter = new MainMenuPresenter();
        MainMenuView mainMenuView = new MainMenuView();
        mainMenuView.setPrompt(prompt);
        mainMenuPresenter.setMainMenuView(mainMenuView);
        loginPresenter.setMainMenuPresenter(mainMenuPresenter);
        signUpPresenter.setMainMenuPresenter(mainMenuPresenter);

        GetBalancePresenter getBalancePresenter = new GetBalancePresenter();
        getBalancePresenter.setCustomerService(customerService);
        GetBalanceView getBalanceView = new GetBalanceView();
        getBalanceView.setPrompt(prompt);
        getBalancePresenter.setGetBalanceView(getBalanceView);
        mainMenuPresenter.setGetBalancePresenter(getBalancePresenter);

        DepositPresenter depositPresenter = new DepositPresenter();
        depositPresenter.setAccountService(accountService);
        AccountTransactionView accountTransactionView = new AccountTransactionView();
        accountTransactionView.setPrompt(prompt);
        mainMenuPresenter.setDepositPresenter(depositPresenter);

        WithdrawPresenter withdrawPresenter = new WithdrawPresenter();
        withdrawPresenter.setAccountService(accountService);
        withdrawPresenter.setAccountTransactionView(accountTransactionView);
        mainMenuPresenter.setWithdrawPresenter(withdrawPresenter);

        TransferencePresenter transferencePresenter = new TransferencePresenter();
        transferencePresenter.setAccountService(accountService);
        TransferenceView transferenceView = new TransferenceView();
        transferenceView.setPrompt(prompt);
        transferencePresenter.setTransferenceView(transferenceView);
        mainMenuPresenter.setTransferencePresenter(transferencePresenter);

        OpenAccountPresenter openAccountPresenter = new OpenAccountPresenter();
        openAccountPresenter.setCustomerService(customerService);
        openAccountPresenter.setAccountService(accountService);
        OpenAccountView openAccountView = new OpenAccountView();
        openAccountView.setPrompt(prompt);
        openAccountPresenter.setOpenAccountView(openAccountView);
        AccountFactory accountFactory = new AccountFactory();
        openAccountPresenter.setAccountFactory(accountFactory);
        mainMenuPresenter.setOpenAccountPresenter(openAccountPresenter);

        return loginPresenter;
    }

}
