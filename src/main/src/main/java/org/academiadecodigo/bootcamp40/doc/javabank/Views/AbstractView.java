package org.academiadecodigo.bootcamp40.doc.javabank.Views;

import org.academiadecodigo.bootcamp.Prompt;

import java.text.DecimalFormat;

public abstract class AbstractView {

    protected Prompt prompt;
    protected DecimalFormat df = new DecimalFormat("#.##");

    public void setPrompt(Prompt prompt) {
        this.prompt = prompt;
    }

}
